
import './App.css';
import Clock from './features/clock/Clock'
import Buttons from './features/button/Buttons';
import { makeStyles } from '@material-ui/core';
import Laps from './features/lap/Laps';

const useStyle = makeStyles({
  center: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  }
})
function App() {
  const classes = useStyle()
  return (
    <div className={classes.center}>
      <Clock />
      <Buttons />
      <Laps />
    </div>
  );
}

export default App;
