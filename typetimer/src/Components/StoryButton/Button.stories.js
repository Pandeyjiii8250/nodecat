import React from 'react'
import Button from './Button'
import {action, actions} from '@storybook/addon-actions'

export default {
    title : "Button3",
    component: Button,
    args: {
        children: 'Button'
    },
    argsTypes  : {
        //controls addOn
        varient: {control : 'text'},
        //forClick addOn
        onclick : {action : 'clicked'}
    }
}

//  export const Primary = () => <Button varient="primary">Primary</Button>
 export const Secondary = () => <Button varient="secondary" onclick={()=> console.log("This is Clicked")}>Secondary</Button>
 export const Success = () => <Button varient="success" onclick={action("Click handler")}>Success</Button>
 export const Danger = () => <Button varient="danger" {...actions('onClick', 'onMouseOver')}>Danger</Button>


 //Args mechanism v6
//  const Template = args => <Button {...args}/>

//  export const PrimaryS = Template.bind({})
//  PrimaryS.args = {
//      varient : 'primary',
//     //  children : 'Primary Args'
//  }

//  export const SecondaryS = Template.bind({})
//  SecondaryS.args = {
//      varient : 'secondary',
//     //  children : 'Secondary Args'
//  }

