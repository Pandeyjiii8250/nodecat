import ResetBtnUI from "../ResetBtnUI";

export default {
    title: "Button/ResetBtn",
    component: ResetBtnUI
}

const Template = args => <ResetBtnUI {...args}/>

export const Reset = Template.bind({})
 