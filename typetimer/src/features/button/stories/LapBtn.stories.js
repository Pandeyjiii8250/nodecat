import LapBtnUI from "../LapBtnUI";

export default {
    title: "Button/LapBtn",
    component: LapBtnUI
}

const Template = args => <LapBtnUI {...args} />

export const Lap = Template.bind({})
Lap.args = {
    start : false
}