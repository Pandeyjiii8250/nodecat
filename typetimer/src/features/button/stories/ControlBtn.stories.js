
import ControlBtnUI from "../ControlBtn.UI"
// import { action } from "@storybook/addon-actions"
// import { boolean } from "@storybook/addon-knobs"

export default {
    title : "Button/ControlBtn",
    component: ControlBtnUI,
    argTypes: {
        onClick: { action: 'Clicked'}
    }
}

const Template = args => <ControlBtnUI {...args} />

export const Play = Template.bind({})
Play.args = {
    start: false,
}
export const Pause = Template.bind({})
Pause.args = {
    start: true
}
