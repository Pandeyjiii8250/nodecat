import React from "react";
import ControlBtn from "./ControlBtn";
import LapBtn from "./LapBtn";
import ResetBtn from "./ResetBtn";

import { withStyles } from "@material-ui/styles";

// import { makeStyles } from "@material-ui/core";
// import { connect } from "react-redux";

const useStyle = () => ({
    btns: {
        display: 'flex',
        margin: '2rem 14rem 0 14rem',
        minWidth: '50vw',
        justifyContent: 'space-around',
    }
}) 
class Buttons extends React.Component{
    // const classes = useStyle()

    render(){
        const { classes } = this.props
        return (
            <div className={classes.btns}>
                <ControlBtn />
                <LapBtn />
                <ResetBtn />
            </div>
        )
    }
}

export default withStyles(useStyle)(Buttons);