import Fab from '@material-ui/core/Fab';
import RotateLeftIcon from '@material-ui/icons/RotateLeft';

function ResetBtnUI({handleReset}) {
    return (
        <Fab color="secondary" aria-label="edit" onClick={handleReset}>
                <RotateLeftIcon fontSize="large"/>
            </Fab>
    )
}

export default ResetBtnUI
