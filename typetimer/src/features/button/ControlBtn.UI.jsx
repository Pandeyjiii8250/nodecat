import React from 'react'
import Fab from '@material-ui/core/Fab';
import PlayArrowOutlinedIcon from '@material-ui/icons/PlayArrowOutlined';
import PauseOutlinedIcon from '@material-ui/icons/PauseOutlined';


function ControlBtnUI({start, handleStart, handleStop}) {
    return (
        <Fab color="secondary" aria-label="edit" onClick={start ? handleStop : handleStart}>
                {start ?
                    <PauseOutlinedIcon  fontSize="large" /> :
                    <PlayArrowOutlinedIcon fontSize="large" />
                }
          </Fab>
    )
}

export default ControlBtnUI
