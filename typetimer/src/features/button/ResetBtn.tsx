import React from 'react';
// import Fab from '@material-ui/core/Fab';
// import RotateLeftIcon from '@material-ui/icons/RotateLeft';
import ResetBtnUI from './ResetBtnUI';
import { resetClock} from '../clock/clockSlice';

import { AppDispatch } from '../../app/store'
import { connect } from 'react-redux';
// import { useAppDispatch} from '../../app/hooks';


interface MyComponentProps {
    dispatch: AppDispatch;
}

class ResetBtn extends React.Component<MyComponentProps> {
    // const dispatch = useAppDispatch()

    constructor(props:MyComponentProps){
        super(props)
        this.handleReset = this.handleReset.bind(this)
    }
    
    handleReset(){
        this.props.dispatch(resetClock())
    }

    render(){
        return (
            <ResetBtnUI handleReset={this.handleReset}/>
        )
    }
}

export default connect()(ResetBtn);


