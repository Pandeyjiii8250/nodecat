import React from 'react'
import Fab from '@material-ui/core/Fab';
import FlagOutlinedIcon from '@material-ui/icons/FlagOutlined';

function LapBtnUI({start, handleClick}) {
    return (
        <Fab color="secondary" aria-label="edit" onClick={handleClick} disabled={!start}>
                <FlagOutlinedIcon fontSize="large"/>
            </Fab>
    )
}

export default LapBtnUI
