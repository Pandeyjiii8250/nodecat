import React from 'react';

import { lapClock } from '../clock/clockSlice';
import LapBtnUI from './LapBtnUI';
import { RootState, AppDispatch } from '../../app/store';
import { connect } from 'react-redux';
// import { useAppDispatch, useAppSelector } from '../../app/hooks';


interface MyComponentProps {
    start: boolean;
    dispatch: AppDispatch;
}

class LapBtn extends React.Component<MyComponentProps> {
    // const start = useAppSelector(state => state.clock.start)
    // const dispatch = useAppDispatch()
    constructor(props:MyComponentProps){
        super(props)
        this.handleClick = this.handleClick.bind(this)
    }

    handleClick(){
        this.props.dispatch(lapClock())
    }

    render(){
        return (
            <LapBtnUI start={this.props.start} handleClick={this.handleClick}/>
        )
    }
}

const mapStateToProps = (state:RootState) => ({
    start: state.clock.start
});

export default connect(mapStateToProps)(LapBtn)