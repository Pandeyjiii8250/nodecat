import React from'react';
// import Fab from '@material-ui/core/Fab';
// import PlayArrowOutlinedIcon from '@material-ui/icons/PlayArrowOutlined';
// import PauseOutlinedIcon from '@material-ui/icons/PauseOutlined';
import { startClock, stopClock } from '../clock/clockSlice';
import ControlBtnUI from './ControlBtn.UI'
//types
import { RootState, AppDispatch } from '../../app/store'

//this can be used for class component
import { connect } from 'react-redux';

//this can be used in functional component only
// import { useAppDispatch, useAppSelector } from '../../app/hooks';


interface MyComponentProps {
    start: boolean;
    dispatch: AppDispatch;
}

class ControlBtn extends React.Component<MyComponentProps> {
    // const start = useAppSelector(state => state.clock.start)
    // const dispatch = useAppDispatch()

    constructor(props:MyComponentProps){
        super(props)
        this.handleStart = this.handleStart.bind(this)
        this.handleStop = this.handleStop.bind(this)
        
    }

    handleStart(){
        this.props.dispatch(startClock())
    }

    handleStop(){
        this.props.dispatch(stopClock())
    }

    render(){
        return (
            <ControlBtnUI start={this.props.start} handleStart={this.handleStart} handleStop={this.handleStop}/>
        )
    }
}

const mapStateToProps = (state: RootState)  => ({
    start: state.clock.start,
});


export default connect(mapStateToProps)(ControlBtn);