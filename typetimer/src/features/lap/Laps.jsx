import { useAppSelector } from "../../app/hooks"
import Table from "./Table"

export default function Laps() {
    const laps = useAppSelector(state => state.clock.lap)
    return (
        <>
          {
              laps.length !== 0 ?
              <Table />:
              <p></p>
          }  
        </>
    )
}
