
export default function TableRow({lap, lapTime, index }) {
    return (
        <tr>
            {console.log(index)}
            <td>{index}</td>
            <td>
                {
                    lap.hrs < 10 ?
                    `0${lap.hrs}`:
                    lap.hrs
                }:
                {
                   lap.min < 10 ?
                   `0${lap.min}`:
                    lap.min
                }: 
                {
                    lap.second < 10 ?
                    `0${lap.second}`:
                    lap.second
                }.
                {   
                    lap.milli < 10 ?
                    `0${lap.milli}`:
                    lap.milli
                }
            </td>
            <td>
                {
                    lapTime.hrs < 10 ?
                    `0${lapTime.hrs}`:
                    lapTime.hrs
                }:
                {
                   lapTime.min < 10 ?
                   `0${lapTime.min}`:
                   lapTime.min
                }: 
                {
                    lapTime.second < 10 ?
                    `0${lapTime.second}`:
                    lapTime.second
                }.
                {   
                    lapTime.milli < 10 ?
                    `0${lapTime.milli}`:
                    lapTime.milli
                }
            </td>
        </tr>
    )
}
