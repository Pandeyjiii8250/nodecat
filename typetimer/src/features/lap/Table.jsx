import { useAppSelector } from "../../app/hooks"
import TableRow from "./TableRow"
import { makeStyles } from "@material-ui/core"

const useStyle = makeStyles({
    container: {
        margin: '2em',
        color: 'white',
        width: '75vw',
        '& table': {
            width: '100%',
            textAlign: 'center',
            borderSpacing: '25px',
        }
    }
})

export default function Table() {
    const laps = useAppSelector(state => state.clock.lap)
    const lapTime = useAppSelector(state => state.clock.freezeTime)
    const classes = useStyle()
    return (
        <div className={classes.container}>
            <table>
                {console.log(laps)}
                <tr>
                    <th>Sr. No. </th>
                    <th> Lap </th>
                    <th> Current Time</th>
                </tr>
                <tbody>
                    {
                        laps.map((lap, index)=>{
                            return <TableRow index={index} lap={lap} lapTime={lapTime[index]} index={index} />
                        })
                    }
                </tbody>
            </table>
        </div>
    )
}
