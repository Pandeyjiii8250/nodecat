import { createSlice } from "@reduxjs/toolkit"

export interface Time {
    hrs: number,
    min: number,
    second: number,
    milli: number
}
interface Clock {
    start: boolean,
    stateTime: number,
    time : Time,
    lastLap: number,
    lap: Time[],
    freezeTime: Time[],
}

const initialState: Clock = {
    start: false,
    stateTime: 0,
    time: {
        hrs: 0,
        min: 0,
        second: 0,
        milli: 0
    },
    lastLap: 0,
    lap: [],
    freezeTime: [],
}

export const clockSlice = createSlice({
    name: 'Clock',
    initialState,
    reducers: {
        startClock : (state) => {
            if(!state.start){
                state.stateTime = Math.floor(Date.now()/10) -
                (state.time.hrs*360000+state.time.min*6000
                    + state.time.second*100+ state.time.milli)
            }
            state.lastLap = state.stateTime + state.lastLap
            state.start = true
        },
        stopClock : (state) => {
            state.start = false
            state.lastLap = state.lastLap - state.stateTime
        },
        resetClock : (state) => {
            const startTime = {
                hrs: 0,
                min: 0,
                second: 0,
                milli: 0
            }
            state.start =  false
            state.stateTime = 0
            state.time = startTime
            state.freezeTime = []
            state.lastLap = 0
            state.lap = []
        },
        lapClock: (state) => {
            const timeInMilli = Math.floor(Date.now()/10)
            let lapTime = 0
            if(state.lastLap !== 0){
                lapTime = timeInMilli - state.lastLap
            }else{
                lapTime = timeInMilli - state.stateTime
            }
            const newTime = calTimeFromMIlli(lapTime)
            state.lastLap = timeInMilli
            state.freezeTime.push(state.time)
            state.lap.push(newTime)
        },

        updateClock: (state) => {
            const latestTime = Math.floor(Date.now()/10)
            const newTime = calTimeFromMIlli(latestTime - state.stateTime)
            state.time = newTime
        }
    }
})

const calTimeFromMIlli = (lapTime: number): Time => {
    let lapHrs = 0
    let lapMin = 0
    let lapsec = 0
    let lapMilli = 0
    if (lapTime >= 360000 ){
        lapHrs = Math.floor(lapTime/360000)
        lapTime = lapTime % 360000
        if(lapTime >= 6000){
            lapMin = Math.floor(lapTime/6000)
            lapTime = lapTime % 6000
            if(lapTime >= 100){
                lapsec = Math.floor(lapTime/100)
                lapMilli = lapTime % 100  
            }else{
                lapsec = 0
                lapMilli = lapTime
            }
        }else{
            lapMin = 0
            if(lapTime >= 100){
                lapsec = Math.floor(lapTime/100)
                lapMilli = lapTime % 100  
            }else{
                lapsec = 0
                lapMilli = lapTime
            }
        }
    }else{
        lapHrs = 0
        if(lapTime >= 6000){
            lapMin = Math.floor(lapTime/6000)
            lapTime = lapTime % 6000
            if(lapTime >= 100){
                lapsec = Math.floor(lapTime/100)
                lapMilli = lapTime % 100  
            }else{
                lapsec = 0
                lapMilli = lapTime
            }
        }else{
            lapMin = 0
            if(lapTime >= 100){
                lapsec = Math.floor(lapTime/100)
                lapMilli = lapTime % 100  
            }else{
                lapsec = 0
                lapMilli = lapTime
            }
        }
    }
    return {
        hrs: lapHrs,
        min: lapMin,
        second: lapsec,
        milli: lapMilli,
    }
} 
export const{ startClock, stopClock, resetClock, lapClock, updateClock} = clockSlice.actions

export default clockSlice.reducer

