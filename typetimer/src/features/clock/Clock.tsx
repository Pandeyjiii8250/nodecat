import React from 'react';
// import {useEffect} from 'react'
import { updateClock } from './clockSlice'

// import { compose } from 'redux';
import { connect } from 'react-redux';
// import { createStyles, withStyles, WithStyles } from "@material-ui/styles";

import { RootState, AppDispatch } from '../../app/store';
import { Time } from '../clock/clockSlice';
import './clock.scss'
// import { useAppSelector, useAppDispatch } from '../../app/hooks'
// import {makeStyles} from '@material-ui/core'


interface MyComponentProps {
    start: boolean;
    currTime: Time;
    // theme: WithStyles<typeof useStyle>;
    dispatch: AppDispatch;
}

// const useStyle = () => createStyles({
//     dispClockMili: {
//         display: 'flex',
//         flexDirection: 'column-reverse',
//         minWidth: '3em',
//         '& h5': {
//             color: 'white',
//             fontSize: '2em',
//             fontWeight: '300',
//             margin: '0 0 2em 0',
//         }
//     },
//     dispClock: {
//         display: 'flex',
//         justifyContent: 'center',
//     },
//     dispTime: {
//         display: 'flex',
//         flexDirection: 'column',
//         alignItems: 'center',
//         '& h1': {
//             color: 'white',
//             fontSize: '8em',
//             fontWeight: '300',
//             margin: 0,
//         },
//         '& p': {
//             color: 'white',
//             margin: '0',
//             fontSize: '2em',
//         },
//     }
// })


class Clock extends React.Component<MyComponentProps> {
    // const start = useAppSelector(state => state.clock.start)
    // const currTime = useAppSelector(state => state.clock.time)
    // const dispatch = useAppDispatch()
    // const classes = useStyle()

    private timerId : ReturnType< typeof setInterval> | null = null;

    

    componentDidUpdate(){
        // console.log("Update")
        if (this.props.start){
            this.timerId = setInterval(()=>{
                this.props.dispatch(updateClock())
            }, 10)
        }
    }

    componentWillUpdate(){
        // console.log(" Before Update")
        if (this.timerId) {
            clearInterval(this.timerId)
        }
    }

    // useEffect(() => {
    //     let timerId : any
    //     if (start){
    //         timerId = setInterval(()=>{
    //             dispatch(updateClock())
    //         }, 10)
    //     }
    //     return () => {
    //         if (timerId)
    //         clearInterval(timerId)
    //     }
    // }, [currTime, start])


    render() {
        // const { classes } = this.props.theme

        return (
            <>
                <div className='dispClock'>
                    <div className='dispTime'>
                        <h1>
                            {
                                this.props.currTime.hrs < 10 ? 
                                `0${this.props.currTime.hrs}` :
                                this.props.currTime.hrs
                            }
                        </h1>
                        <p>hr</p>
                    </div>
                    <div className='dispTime'>
                        <h1>:</h1>
                    </div>
                    <div className='dispTime'>
                        <h1>
                            {
                                this.props.currTime.min < 10 ? 
                                `0${this.props.currTime.min}` :
                                this.props.currTime.min
                            }
                        </h1>
                        <p>min</p>
                    </div>
                    <div className='dispTime'>
                        <h1>:</h1>
                    </div>
                    <div className='dispTime'>
                        <h1>
                            {
                                this.props.currTime.second < 10 ? 
                                `0${this.props.currTime.second}` :
                                this.props.currTime.second
                            }
                        </h1>
                        <p>sec</p>
                    </div>
                    <div className='dispTime'>
                        <h1>.</h1>
                    </div>
                    <div className="dispClockMili">
                        <h5>
                            {
                                this.props.currTime.milli < 10 ? 
                                `0${this.props.currTime.milli}`  :
                                this.props.currTime.milli
                            }
                        </h5>
                    </div>
                </div>
            </>
        )
    }
}

const mapStateToProps = (state : RootState) => ({
    start : state.clock.start,
    currTime :  state.clock.time,
});

// export default compose(
//     // withStyles(useStyle),
//     connect( mapStateToProps, null)
// )(Clock);

export default connect( mapStateToProps )(Clock);




