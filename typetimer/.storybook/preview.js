import React from 'react'
import Center from '../src/Components/Center/Center'
import { addDecorator, addParameters } from '@storybook/react'
// import {withConsole } from '@storybook/addon-console'
import {withKnobs} from "@storybook/addon-knobs"
// import {withA11y} from "@storybook/addon-a11y"
// import { INITIAl_VIEWPORTS } from "@storybook/addon-viewport"

import { store } from '../src/app/store'
import { Provider } from 'react-redux';
//decorator for v5
// addDecorator(story => <Center>{story()}</Center>)

//decorator for v6
// addDecorator((storyFn, context) => withConsole()(storyFn)(context))
export const decorators = [
  (story) => (
    <Provider store={store}>
      <Center>{story()}</Center>
    </Provider>
  )
]
addDecorator(withKnobs)
// addDecorator(withA11y)

// addParameters({
//   viewport: {
//     viewports: INITIAl_VIEWPORTS,
//   }
// })


export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}

//ENVIRONMENT VARIABLE
//set STORYBOOK_THEME = dark        .....in script
//process.env.STORYBOOK_THEME