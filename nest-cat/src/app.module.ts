import { Module } from '@nestjs/common';
// import { AppController } from './app.controller';
// import { CatController } from './catModule/cat.controller';
// import { CatService } from './catModule/cat.service';
// import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';
// import { AuthModule } from './auth/auth.module';
// import { loadTypeOrmConnectionFromEnv } from './loader';
// import { CatEntity } from './db/entities/cat.entity';
import { CatModule } from './catModule/cat.module';

@Module({
  imports: [
    CatModule,
    AuthModule,
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'data/dev.db',
      entities: [__dirname + '**/*.entity.js}'],
      synchronize: false,
      autoLoadEntities: true,
      // migrations : ["src/**/*.ts"],
    }),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
