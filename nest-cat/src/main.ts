import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
// import { loadEnvironmentVarible } from './loader';

async function bootstrap() {
  // await loadEnvironmentVarible()
  const app = await NestFactory.create(AppModule);
  await app.listen(3000);
}
bootstrap();
