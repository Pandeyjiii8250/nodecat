import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class catTable1631004042529 implements MigrationInterface {

    public catTable: Table = new Table({
        name: "cats",
        columns: [
            {
                name: "id",
                type: "string",
                isGenerated: true,
                isPrimary: true,
                generationStrategy: "uuid"
            },
            {
                name: "name",
                type: "string",
                isNullable: false
            },
            {
                name: "age",
                type: "integer",
                isNullable: false,
                isUnique: false
            },
            {
                name: "breed",
                type: "string",
                isNullable: false,
                isUnique: false
            }
        ]
    })

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(this.catTable)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable(this.catTable)
    }

}
