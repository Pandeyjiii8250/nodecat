import {Column, Entity, PrimaryColumn} from "typeorm";

@Entity({name:'cats'})
export class CatEntity {
    @PrimaryColumn()
    id: string;

    @Column()
    name: string;

    @Column()
    age: number;

    @Column()
    breed: string;
}
