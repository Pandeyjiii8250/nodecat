import {Injectable} from '@nestjs/common'
import { JwtService } from '@nestjs/jwt';
// import { Observable } from 'rxjs';
import { CatService } from 'src/catModule/cat.service';
// const bcrypt = require('bcrypt') 

@Injectable()
export class AuthService {
    constructor(
        private catUserService: CatService,
        private jwtService: JwtService
    ){}

    async validateUser(username: string, pass: string): Promise<any> {
        const user = await this.catUserService.findOne(username)
        if (user && user.password === pass) {
            const {password, ...result} = user
            return result;
        }
        return null
    }

    async login(user: any){
        const payload = {username: user.username, sub: user.userId}
        return{
            access_token : this.jwtService.sign(payload)
        }
    }

    // generateJWT(): Promise<string>{
    //     return this.jwtService.signAsync('testUser')
    // }
    // hashpassword(password:string): Observable<string>{
    //     return bcrypt.hash(password, 12)
    // }

    // comparePass(password:string, storedPass: string): Observable<any>{
    //     return bcrypt.compare(password, storedPass)
    // }
}