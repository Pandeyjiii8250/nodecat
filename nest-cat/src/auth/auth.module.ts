import { Module } from "@nestjs/common";
import { JwtModule } from "@nestjs/jwt";
// import { ConfigModule, ConfigService } from "@nestjs/config";
// import { JwtModule } from "@nestjs/jwt";
import { PassportModule } from "@nestjs/passport";
import { CatModule } from "src/catModule/cat.module";
import { AuthController } from "./auth.controller";
import { AuthService } from "./auth.service";
import { jwtConstants } from "./constants";
import { JwtStrategy } from "./stratgies/jwt-strategy";
// import { jwtConstants } from "./constants";
// import { JwtAuthGuard } from "./gaurd/jwt-auth.gard";
import { LocalStrategy } from "./stratgies/local-strategy";

@Module({
    imports: [
        PassportModule,
        CatModule,
        JwtModule.register({
            secret: jwtConstants.secret,
            signOptions: {expiresIn: '10000'}
        })
    ],
    controllers: [AuthController],
    providers: [AuthService, LocalStrategy, JwtStrategy],
    exports: [AuthService],
})
export class AuthModule {}