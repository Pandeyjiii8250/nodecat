import { Controller, Post, Req, UseGuards } from "@nestjs/common";
// import { AuthGuard } from "@nestjs/passport";
import { AuthService } from "./auth.service";
import { LocalAuthGuard } from "./gaurd/local-auth.gard";


@Controller()
export class AuthController {
    constructor(private serve: AuthService){}
    
    @UseGuards(LocalAuthGuard)
    @Post('auth/login')
    async login(@Req() req){
        return this.serve.login(req.user)
    }
    

}