import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { CatController } from "./cat.controller";
import { CatRepository } from "./cat.repository";
import { CatService } from "./cat.service";


@Module({
    imports: [
        TypeOrmModule.forFeature([CatRepository])
    ],
    controllers: [CatController],
    providers: [CatService],
    exports: [CatService]
})
export class CatModule {}