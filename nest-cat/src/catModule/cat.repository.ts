import { CatEntity } from "src/db/entities/cat.entity";
import { EntityRepository, Repository } from "typeorm";
import { CatDto } from "./cat.dto";


@EntityRepository(CatEntity)
export class CatRepository extends Repository<CatEntity> {
    async createCat(catDto: CatDto) : Promise<CatEntity> {
        const {name, age, breed} = catDto
        const cat = this.create({
            name,
            age,
            breed,
        });
        await this.save(cat)
        return cat
    }
}