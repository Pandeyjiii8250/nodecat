import * as Joi from "joi"

export const catSchmea = Joi.object({
    name: Joi.string().pattern(/^[\sa-zA-z]+$/).required(),
    age: Joi.number().max(17).required(),
    breed: Joi.string().pattern(/^[\sa-zA-z]+$/).required()
})

