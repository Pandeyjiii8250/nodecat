import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { randomUUID } from "crypto";
import { CatEntity } from "src/db/entities/cat.entity";
import { CatDto } from "./cat.dto";
import { CatRepository } from "./cat.repository";

export type User = any
@Injectable()
export class CatService {
  private readonly users = [
    {
      userId: 1,
      username: 'Vishwas',
      password: 'Bharosa',
    },
    {
      userId: 2,
      username: 'Soumik',
      password: 'Patanahi',
    },
  ]
  constructor(@InjectRepository(CatRepository) private catRepo: CatRepository) {}

  async findOne(username: string): Promise<User | undefined> {
    return this.users.find(user=> user.username === username)
  }
  getAllCat(): Promise<CatEntity[]> {
    return this.catRepo.find();
  }

  getCatById(id:string):Promise<CatEntity> {
    return this.catRepo.findOne(id) //findOneOrFail
  }

  async createCat(catDto: CatDto): Promise<CatEntity> {
    const cat = this.catRepo.create({
      id:randomUUID(),
      name: catDto.name,
      age: catDto.age,
      breed: catDto.breed
    })
    return await this.catRepo.save(cat)
  }

  async updateCatById(catDto: CatDto, id:string): Promise<CatEntity>{
    const cat = await this.catRepo.findOne(id)
    cat.name = catDto.name
    cat.age = catDto.age
    cat.breed = catDto.breed
    return await this.catRepo.save(cat)
  }

  async delCatById(id:string): Promise<CatEntity> {
    const cat = await this.catRepo.findOne(id)
    return this.catRepo.remove(cat)
  }

  async searchByAge(agelte:number, agegte:number):Promise<CatEntity[]>{
    const cats = await this.catRepo.query(
      'SELECT * FROM cats WHERE age>=? and age<=?',
    [agelte, agegte]
    )
    if(cats){
      return cats
    }else{
      throw new Error("Some Thing Went Wrong")
    }
  }
}