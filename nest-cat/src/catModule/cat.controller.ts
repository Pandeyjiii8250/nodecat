import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards, UsePipes } from "@nestjs/common";
import { JwtAuthGuard } from "src/auth/gaurd/jwt-auth.gaurd";
import { LocalAuthGuard } from "src/auth/gaurd/local-auth.gard";
import { CatDto } from "src/catModule/cat.dto";
import { CatEntity } from "src/db/entities/cat.entity";
import { catSchmea } from "./cat.joi.schema";
import { CatService } from "./cat.service";
import { ParamsValidatePipe, QueryValidatePipe, ValidatePipe } from "./validation.pipe";
// import * as Joi from 'joi'

@Controller()
export class CatController {
    constructor(private catservice: CatService) {}

    @Get()
    getAll():Promise<CatEntity[]> {
        return this.catservice.getAllCat()
    }

    @Get('search')
    getCatAge
    (
        @Query('age_lte', new QueryValidatePipe()) agelte, 
        @Query('age_gte', new QueryValidatePipe()) agegte
    ): Promise<CatEntity[]>{
        return this.catservice.searchByAge(agelte, agegte)
    }
    
    @UseGuards(LocalAuthGuard)
    @Get(':id')
    getById(@Param('id', new ParamsValidatePipe()) id: string):Promise<CatEntity> {
        return this.catservice.getCatById(id)
    }

    @Post()
    @UsePipes(new ValidatePipe(catSchmea))
    postCat(@Body() catDto: CatDto): Promise<CatEntity> {
        return this.catservice.createCat(catDto);
    }
    // @UseGuards(JwtAuthGuard)
    @Put(':id')
    @UsePipes(new ValidatePipe(catSchmea))
    updateById(@Param('id', new ParamsValidatePipe()) id: string, @Body() catDto: CatDto): Promise<CatEntity> {
        return this.catservice.updateCatById(catDto, id)
    }

    @Delete(':id')
    delById(@Param('id', new ParamsValidatePipe()) id: string): Promise<CatEntity> {
        return this.catservice.delCatById(id)
    }

}