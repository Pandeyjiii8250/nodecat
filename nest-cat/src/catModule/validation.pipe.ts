import { ArgumentMetadata, BadRequestException, Injectable, PipeTransform } from "@nestjs/common";
import { ObjectSchema } from "joi";
import * as Joi from 'joi'


@Injectable()
export class ValidatePipe implements PipeTransform {
    constructor(
        private schema: ObjectSchema,
        private querySchema = Joi.number(),
        private paraSchema = Joi.string().guid({version:["uuidv4", "uuidv5"]})
    ){}

    transform(value: any, metadata: ArgumentMetadata) {
        let result
        if (metadata.type === 'body'){
            result = this.schema.validate(value)
        }else if(metadata.type === 'query'){
            result = this.querySchema.validate(value)
        }else{
            result = this.paraSchema.validate(value)
        }
        if(result.error){
            throw new BadRequestException('Validation Failed')
        }else{
            return value
        }
    }
}

export class ParamsValidatePipe implements PipeTransform {
    constructor(private schema = Joi.string().guid({version:["uuidv4", "uuidv5"]})){}

    transform(value:any, metadata: ArgumentMetadata){
        const {error} = this.schema.validate(value)
        if(error){
            throw new BadRequestException('Validation Failed')
        }else{
            return value
        }
    }
}

export class QueryValidatePipe implements PipeTransform {
    constructor(private schema = Joi.number()){}

    transform(value:any, metadata: ArgumentMetadata){
        const {error} = this.schema.validate(value)
        if(error){
            throw new BadRequestException('Validation Failed')
        }else{
            return value
        }
    }
}