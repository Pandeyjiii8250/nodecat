const { Router } = require('express')
const {
    getAllCats, 
    newCat,
    getCatById,
    updateCatById,
    delCatById,
    searchByAge,
    verifyCat
} = require('../controller/catController')
const router = Router()


router.get('/', getAllCats)
router.get('/search', searchByAge)
router.get('/:id', getCatById)
router.post('/', verifyCat, newCat)
router.put('/:id', verifyCat, updateCatById)
router.delete('/:id', delCatById)


module.exports = router