const express = require('express')
const cors = require('cors')
const database = require('./database/connect')
const catRoute = require('./routes/catRoutes')


database.connect((err)=>{
    if (err){
        console.log("Databsee not connected" + err.message)
    }else{
        console.log("Db Connected")
    }
})
const app  = express()

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({
    extended: true
}))

app.use(catRoute)

app.listen(3000, ()=>{
    console.log("Running")
})