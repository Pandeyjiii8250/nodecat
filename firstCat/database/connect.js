const mysql = require('mysql')
require('dotenv').config();
//cpy .dev.env into your .env
const database = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_DATABASE
})

module.exports = database