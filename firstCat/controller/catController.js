const db = require('../database/connect') 
const { v4 : uuidv4 } = require('uuid');
const e = require('express');

const verifyCat = (req, res, next)=>{
    const {name, age, breed} = req.body
    let namePattern = name.match(/[\sa-zA-z]+/)
    let breedPattern = breed.match(/[\sa-zA-z]+/)
    console.log(name===namePattern.input)
    if (age <= 17 && namePattern.input === name && breedPattern.input === breed){
        next()
    }else{
        if(age>17){
            res.send("Age should be less than 17").status(200)
        }else if(namePattern.input !== name){
            res.send("Please enter correct name.").status(200)
        }else if(breedPattern.input !== breed){
            res.send("Please enter correct breed").status(200)
        }
    }
}

const getAllCats = (req, res, next)=>{
    db.query(
        'SELECT * FROM cat',
        (err, result)=>{
            if (err){
                next(err)
            }else{
                res.send(result)
            }
        }
     )
}

const newCat = (req, res, next)=>{
    const {name, age, breed} = req.body
    console.log(name, age, breed)
    db.query(
        'INSERT INTO cat (id,Name, Age, Breed) VALUES(?,?,?,?)',
        [uuidv4(), name, age, breed], 
        (err, result)=>{
            if (err){
                next(err)
            }else{
                res.status(201).send(result)
            }
        }
    )
}

const getCatById = (req, res, next)=>{
    const id = req.params.id
    console.log(id)
    db.query(
        'SELECT * FROM cat WHERE id=?',
        [id],
        (err, result)=>{
            if(err){
                console.log(err)
                next(err)
            }else{
                console.log(result)
                res.status(200).send(result)
            }
        }
    )
}

const updateCatById = (req, res, next)=>{
    const id = req.params.id
    const {name, age, breed} = req.body
    // console.log(res)

    db.query(
        'UPDATE cat SET name=?, age=?, breed=? WHERE id=?',
        [name, age, breed, id],
        (err, result)=>{
            if(err){
                next(err)
            }else{
                res.status(201).send(result)
            }
        }
    )
}

const delCatById = (req, res, next)=>{
    const id = req.params.id
    // console.log(id)
    db.query(
        'DELETE FROM cat WHERE id=?',
        [id],
        (err, result)=>{
            if(err){
                next(err)
            }else{
                if(result.affectedRows === 0){
                    res.send("No cat Found").status(200)
                }else{
                    res.status(200).send()
                }
            }
        }
    )
}

const searchByAge = (req, res, next)=>{
    const {age_lte, age_gte} = req.query
    if(!isNaN(age_gte) && !isNaN(age_lte)){
        db.query(
            'SELECT * FROM cat WHERE age>=? AND age<=?',
            [age_lte, age_gte],
            (err, result)=>{
                if(err){
                    next(err)
                }else{
                    res.status(200).send(result)
                }
            }
        )
    }else{
        res.send('Enter Integer Value').status(200)
    }
}

module.exports = {
    getAllCats,
    newCat,
    getCatById,
    updateCatById,
    delCatById,
    searchByAge,
    verifyCat
}




